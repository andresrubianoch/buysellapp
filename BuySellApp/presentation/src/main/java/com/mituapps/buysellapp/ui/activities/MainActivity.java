package com.mituapps.buysellapp.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.mituapps.buysellapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        injectButterKnifeView();
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
