package com.mituapps.buysellapp.view;


/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public interface MainView extends BaseView {

    void showLoading();
    void hideLoading();
    void enableInputs(boolean status);
    void showErrorMessage(String message);
    void noInternetConnection();

    boolean validateFields();

}
