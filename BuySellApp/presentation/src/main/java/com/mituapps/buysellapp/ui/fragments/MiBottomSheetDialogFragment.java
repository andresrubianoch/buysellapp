package com.mituapps.buysellapp.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mituapps.buysellapp.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiBottomSheetDialogFragment extends BottomSheetDialogFragment {

    private OnFragmentInteractionListener mListener;
    public static final byte CAMERA = 1;
    public static final byte GALLERY = 2;

    public static MiBottomSheetDialogFragment newInstance() {
        return new MiBottomSheetDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_modal_sheet, container, false);
        ButterKnife.bind(this, v);
        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.buttonCamera, R.id.buttonGallery})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonCamera:
                    onButtonPressed(CAMERA);
                break;
            case R.id.buttonGallery:
                    onButtonPressed(GALLERY);
                break;
        }
    }

    public void onButtonPressed(byte type) {
        if (mListener != null) {
            mListener.onFragmentInteraction(type);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(byte type);
    }



}