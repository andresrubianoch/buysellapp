package com.mituapps.buysellapp.utils.navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.mituapps.buysellapp.ui.activities.DetailImageActivity;
import com.mituapps.buysellapp.ui.activities.PhotosActivity;
import com.mituapps.buysellapp.ui.activities.SendActivity;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public class Navigator {

    public Navigator() {
    }

    private Intent getIntent(Context context, @NonNull Class goClass){
        return getIntent(context, goClass, null);
    }

    private Intent getIntent(Context context, @NonNull Class goClass, Bundle bundle){
        Intent intent = new Intent(context, goClass);
        if (null != bundle){
            intent.putExtras(bundle);
        }
        return intent;
    }

    public void toPhotoActivity(@NonNull Context context){
        if (null != context){
            context.startActivity(getIntent(context, PhotosActivity.class));
        } else {
            throw new NullPointerException();
        }
    }

    public void toSendActivity(@NonNull Context context, Bundle bundle){
        if (null != context){
            context.startActivity(getIntent(context, SendActivity.class, bundle));
        } else {
            throw new NullPointerException();
        }
    }

    public void toDetailImageActivity(@NonNull Context context, Bundle bundle){
        if (null != context){
            context.startActivity(getIntent(context, DetailImageActivity.class, bundle));
        } else {
            throw new NullPointerException();
        }
    }

}
