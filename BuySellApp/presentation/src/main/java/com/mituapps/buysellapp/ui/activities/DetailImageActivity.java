package com.mituapps.buysellapp.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.mituapps.buysellapp.R;
import com.mituapps.buysellapp.ui.fragments.DetailImageFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailImageActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_image);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initFragment();
    }

    private void initFragment() {
        addFragment(R.id.fragment, new DetailImageFragment(), getIntent().getExtras());
    }

}
