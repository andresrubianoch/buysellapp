package com.mituapps.buysellapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mituapps.buysellapp.R;
import com.mituapps.buysellapp.utils.app.EndPoints;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment extends BaseFragment {

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.container_main)
    LinearLayout containerMain;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        initImage();
        return view;
    }

    private void initImage() {
        Glide.with(getActivity())
                .load(EndPoints.IMAGES.IMAGE_HOME)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.mipmap.ic_launcher)
                .into(image);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }


    @OnClick({R.id.buttonSell, R.id.buttonBuy})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSell:
                onClickSell();
                break;
            case R.id.buttonBuy:
                onClickBuy();
                break;
        }
    }

    private void onClickSell() {
        if (null != mNavigator){
            mNavigator.toPhotoActivity(getActivity());
        }
    }

    private void onClickBuy() {
        showSnackbarMessage(containerMain, "Buy");
    }
}
