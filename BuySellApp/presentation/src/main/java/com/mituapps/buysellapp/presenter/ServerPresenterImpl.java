package com.mituapps.buysellapp.presenter;


import com.interactor.InteractorImpl;
import com.interactor.ServerCallback;
import com.mituapps.buysellapp.view.MainView;
import com.mituapps.data.datasource.DataStoreFactory;
import com.mituapps.data.repository.RepositoryImpl;
import com.repository.ServerRepository;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class ServerPresenterImpl implements Presenter<MainView>, ServerCallback {
    private static final String TAG = "SignInPresenter";
    private static final int ERR_NO_INTERNET = 1;
    private static final int ERR_CREDENTIALS = 2;
    private MainView mainView;
    private InteractorImpl themeInteractor;

    public void onClick() {
        this.mainView.enableInputs(false);
        this.mainView.showLoading();
        if (this.mainView.validateFields()) {
            this.themeInteractor.onUploadPhotos(this);
        }
    }

    @Override
    public void addView(MainView view) {
        this.mainView = view;
        ServerRepository themeRepository = new RepositoryImpl(new DataStoreFactory(this.mainView.getContext()));
        themeInteractor = new InteractorImpl(themeRepository);
    }

    @Override
    public void removeView(MainView view) {
        this.mainView = null;
    }

    @Override
    public void onThemeSuccess() {
        if (null != this.mainView) {
        }
    }

    @Override
    public void onThemeError(String message) {
        if (null != this.mainView) {
        }
    }

}
