package com.mituapps.buysellapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mituapps.buysellapp.R;
import com.mituapps.buysellapp.models.SellImage;
import com.mituapps.buysellapp.utils.media.ImageLoaderHelper;

import java.util.ArrayList;

/**
 * Created by Andres Rubiano Del Chiaro on 19/09/2016.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.RecyclerViewHolder> {

    private ArrayList<SellImage> mItems = new ArrayList<>();
    private ImageLoaderHelper imageLoaderHelper;
    private Context context;

    public PhotoAdapter(Context context) {
        this.context = context;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    public PhotoAdapter(Context context, ArrayList<SellImage> mItems) {
        this.mItems = mItems;
        this.context = context;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new RecyclerViewHolder(v);
    }

    public void addAll(SellImage item) {
        int pos = getItemCount();
        mItems.add(item);
        notifyItemRangeInserted(pos, mItems.size());
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.img_sampleimage);
        }

        public void bind(int position) {
            imageLoaderHelper.getLoader().loadLocal(
                                        mItems.get(position).getPath(),
                                        mImageView);
        }
    }

}
