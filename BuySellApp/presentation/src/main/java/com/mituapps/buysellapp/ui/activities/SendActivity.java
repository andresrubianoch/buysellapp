package com.mituapps.buysellapp.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.mituapps.buysellapp.R;
import com.mituapps.buysellapp.ui.fragments.SendFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SendActivity extends BaseActivity {

    public static final String ARRAY_IMAGES = "arrayImages";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initFragment();
    }

    private void initFragment() {
        Bundle bundle = getIntent().getExtras();
        addFragment(R.id.frameSendActivity, new SendFragment(), bundle);
    }

}
