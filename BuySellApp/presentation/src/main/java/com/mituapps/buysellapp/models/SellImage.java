package com.mituapps.buysellapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andres Rubiano Del Chiaro on 06/10/2016.
 */

public class SellImage implements Parcelable {

    private String path;

    public SellImage() {
    }

    public SellImage(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.path);
    }

    protected SellImage(Parcel in) {
        this.path = in.readString();
    }

    public static final Parcelable.Creator<SellImage> CREATOR = new Parcelable.Creator<SellImage>() {
        @Override
        public SellImage createFromParcel(Parcel source) {
            return new SellImage(source);
        }

        @Override
        public SellImage[] newArray(int size) {
            return new SellImage[size];
        }
    };
}
