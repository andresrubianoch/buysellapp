package com.mituapps.buysellapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.frosquivel.magicalcamera.MagicalCamera;
import com.mituapps.buysellapp.R;
import com.mituapps.buysellapp.adapter.PhotoAdapter;
import com.mituapps.buysellapp.models.SellImage;
import com.mituapps.buysellapp.ui.fragments.DetailImageFragment;
import com.mituapps.buysellapp.ui.fragments.MiBottomSheetDialogFragment;
import com.mituapps.buysellapp.ui.recycler.RecyclerClickListener;
import com.mituapps.buysellapp.utils.app.EndPoints;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotosActivity extends BaseActivity implements MiBottomSheetDialogFragment.OnFragmentInteractionListener {

    @Bind(R.id.recyclerPhoto)
    RecyclerView recyclerPhoto;
    @Bind(R.id.containerPhotos)
    CoordinatorLayout containerPhotos;
    @Bind(R.id.onPhotoContinue)
    Button onPhotoContinue;
    @Bind(R.id.onAddPhoto)
    Button onAddPhoto;

    private ArrayList<SellImage> listPhoto;
    private PhotoAdapter mAdapter;

    private MagicalCamera magicalCamera;
    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 2000;

    private BottomSheetDialogFragment bsdFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bsdFragment = MiBottomSheetDialogFragment.newInstance();
        magicalCamera = new MagicalCamera(PhotosActivity.this, RESIZE_PHOTO_PIXELS_PERCENTAGE);

        hideButton();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initList();
        initRecycler();
        initAdapter();
    }

    private void initRecycler() {
        setUpRecycler(recyclerPhoto,
                new RecyclerClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        String imageRoute = listPhoto.get(position).getPath();
                        Bundle bundle = new Bundle();
                        bundle.putString(DetailImageFragment.BUNDLE_IMAGE_ID, imageRoute);
                        mNavigator.toDetailImageActivity(PhotosActivity.this, bundle);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                });
    }

    private void initAdapter() {
        mAdapter = new PhotoAdapter(this);
        recyclerPhoto.setAdapter(mAdapter);
    }

    private void addItemToAdapter(SellImage sellImage) {
        mAdapter.addAll(sellImage);
    }

    private void onGallery() {
        magicalCamera.selectedPicture("my_header_name");
    }

    private void onCamera() {
        magicalCamera.takePhoto();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //call this method ever
        magicalCamera.resultPhoto(requestCode, resultCode, data);
        String route = "";
        if (requestCode == MagicalCamera.TAKE_PHOTO) {
            boolean isSave = magicalCamera.savePhotoInMemoryDevice(
                    magicalCamera.getMyPhoto(),
                    getNextSequence(),
                    EndPoints.IMAGES.DIRECTORY_NAME,
                    MagicalCamera.PNG,
                    false);
            if (isSave) {
                route = EndPoints.IMAGES.FIRST_ROUTE + EndPoints.IMAGES.DIRECTORY_NAME + getNextSequence() + ".png";
            }
        } else {
            //Gallery
            route = magicalCamera.getRealPath();
        }
        if (route.length() > 0) {
            SellImage object = new SellImage(route);
            listPhoto.add(object);
            addItemToAdapter(object);
            showButton();
            Log.i("image", route);
        }
        enableInput(true);
    }

    @Override
    public void onFragmentInteraction(byte type) {
        if (type == MiBottomSheetDialogFragment.CAMERA) {
            onCamera();
        } else if (type == MiBottomSheetDialogFragment.GALLERY) {
            onGallery();
        }
        bsdFragment.dismiss();
    }

    private void initList() {
        listPhoto = new ArrayList<>();
    }

    private String getNextSequence() {
        return EndPoints.IMAGES.PHOTO_NAME + listPhoto.size();
    }

    private void hideButton() {
        hideView(onPhotoContinue);
    }

    private void showButton() {
        showView(onPhotoContinue);
    }

    @OnClick(R.id.onPhotoContinue)
    public void onPhotoContinue() {
        enableInput(false);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SendActivity.ARRAY_IMAGES, listPhoto);
        mNavigator.toSendActivity(PhotosActivity.this, bundle);
    }

    @OnClick(R.id.onAddPhoto)
    public void onAddPhoto() {
        if (listPhoto.size() < 3) {
            bsdFragment.show(
                    PhotosActivity.this.getSupportFragmentManager(), "BSDialog");

        } else {
            showSnackbarMessage(containerPhotos,
                    "Solamente puede agregar 3 items.");
        }
    }

    private void enableInput(boolean state){
        onPhotoContinue.setEnabled(state);
        onAddPhoto.setEnabled(state);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableInput(true);
    }
}
