package com.mituapps.buysellapp.view;

import android.content.Context;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public interface BaseView {

    Context getContext();
}
