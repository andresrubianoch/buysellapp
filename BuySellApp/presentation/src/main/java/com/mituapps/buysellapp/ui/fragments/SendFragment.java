package com.mituapps.buysellapp.ui.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.mituapps.buysellapp.R;
import com.mituapps.buysellapp.models.SellImage;
import com.mituapps.buysellapp.presenter.ServerPresenterImpl;
import com.mituapps.buysellapp.ui.activities.SendActivity;
import com.mituapps.buysellapp.view.MainView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class SendFragment extends BaseFragment implements MainView {

    @Bind(R.id.spinnerSize)
    Spinner spinnerSize;
    @Bind(R.id.onSend)
    Button onSend;
    @Bind(R.id.content_send)
    RelativeLayout contentSend;
    @Bind(R.id.wrapperWeight)
    TextInputLayout wrapperWeight;
    @Bind(R.id.inputWeight)
    EditText inputWeight;

    private ServerPresenterImpl mServerPresenter;

    private ArrayList<SellImage> listImages;

    public SendFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send, container, false);
        ButterKnife.bind(this, view);
        initArray();
        initSpinner();
        setUpPresenter();
        return view;
    }

    private void setUpPresenter() {
        if (null == mServerPresenter) {
            mServerPresenter = new ServerPresenterImpl();
            mServerPresenter.addView(this);
        }
    }

    private void initArray() {
        Bundle bundle = getArguments();
        listImages = new ArrayList<>();
        listImages.addAll(bundle.<SellImage>getParcelableArrayList(SendActivity.ARRAY_IMAGES));
        for (SellImage image : listImages) {
            Log.i("image", image.getPath());
        }
    }

    private void initSpinner() {
        spinnerSize.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.listSize)));
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        this.mServerPresenter.removeView(SendFragment.this);
        super.onDestroyView();
    }

    @OnClick(R.id.onSend)
    public void onClick() {

        if (validateFields()) {

        }
    }

    @Override
    public boolean validateFields() {
        if (spinnerSize.getSelectedItemPosition() == 0) {
            showErrorMessage(getResources().getString(R.string.error_spinner_size));
            return false;
        }

        if (inputWeight.getText().toString().trim().isEmpty()) {
            wrapperWeight.setError(getResources().getString(R.string.error_field_null));
            requestFocus(inputWeight);
            return false;
        } else {
            wrapperWeight.setErrorEnabled(false);
        }
        return true;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void enableInputs(boolean status) {
        enableInputs(status);
    }

    @Override
    public void showErrorMessage(String message) {
        showSnackbarMessage(contentSend, message);
    }

    @Override
    public void noInternetConnection() {

    }

}
