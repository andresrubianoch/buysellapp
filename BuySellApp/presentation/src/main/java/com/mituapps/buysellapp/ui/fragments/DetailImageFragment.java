package com.mituapps.buysellapp.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.mituapps.buysellapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailImageFragment extends Fragment {

    @Bind(R.id.imageDetail)
    SubsamplingScaleImageView imageDetail;

    private static final String ROUTE = "/storage/emulated/0/Pictures/myDirectoryName/myPhotoName2.png";
    public static final String BUNDLE_IMAGE_ID = "imageRoute";

    public DetailImageFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_image, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        imageDetail.setImage(ImageSource.uri(bundle.getString(BUNDLE_IMAGE_ID)));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
