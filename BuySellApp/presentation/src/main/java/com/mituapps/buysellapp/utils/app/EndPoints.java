package com.mituapps.buysellapp.utils.app;

/**
 * Created by Andres Rubiano Del Chiaro on 06/10/2016.
 */

public class EndPoints {

    public static final class IMAGES {
        public static final String IMAGE_HOME = "http://likecomtic.com/wp-content/uploads/2013/09/Quipux-Innova.jpg";

        public static final String PHOTO_NAME = "photoName";

        public static final String DIRECTORY_NAME = "directoryName/";

        public static final String FIRST_ROUTE = "/mnt/sdcard/Pictures/";
    }
}
