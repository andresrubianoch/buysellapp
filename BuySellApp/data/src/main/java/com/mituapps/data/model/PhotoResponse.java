package com.mituapps.data.model;


/**
 * Created by Andres Rubiano Del Chiaro on 28/09/2016.
 */
public class PhotoResponse extends BaseResponse {

    private int offset;
    private Object nextPage;
    private int totalObjects;

    public PhotoResponse() {
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public Object getNextPage() {
        return nextPage;
    }

    public void setNextPage(Object nextPage) {
        this.nextPage = nextPage;
    }

    public int getTotalObjects() {
        return totalObjects;
    }

    public void setTotalObjects(int totalObjects) {
        this.totalObjects = totalObjects;
    }
}
