package com.mituapps.data.datasource;

import com.repository.RepositoryCallback;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public interface MethodsDataStore {

    void onUploadPhoto(RepositoryCallback repositoryCallback);
}
