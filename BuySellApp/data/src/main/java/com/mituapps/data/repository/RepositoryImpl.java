package com.mituapps.data.repository;

import com.interactor.ServerCallback;
import com.mituapps.data.datasource.DataStoreFactory;
import com.mituapps.data.datasource.MethodsDataStore;
import com.repository.RepositoryCallback;
import com.repository.ServerRepository;

/**
 * Created by Andres Rubiano Del Chiaro  on 27/09/2016.
 */

public class RepositoryImpl implements ServerRepository {

    private final DataStoreFactory dataStoreFactory;

    public RepositoryImpl(DataStoreFactory dataStoreFactory) {
        this.dataStoreFactory = dataStoreFactory;
    }

    @Override
    public void uploadPhotos(final ServerCallback serverCallback) {
        final MethodsDataStore methodsDataStore = this.dataStoreFactory.create(DataStoreFactory.CLOUD);
        methodsDataStore.onUploadPhoto(new RepositoryCallback() {
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                serverCallback.onThemeError(message);
            }

            @Override
            public void onSuccess(Object object) {
                serverCallback.onThemeSuccess();
            }
        });
    }
}
