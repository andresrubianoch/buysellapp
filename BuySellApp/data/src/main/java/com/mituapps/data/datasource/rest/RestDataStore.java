package com.mituapps.data.datasource.rest;

import android.util.Log;

import com.mituapps.data.datasource.MethodsDataStore;
import com.mituapps.data.model.PhotoResponse;
import com.repository.RepositoryCallback;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class RestDataStore implements MethodsDataStore {

    private static final String TAG = "RestPlaceDataS";
    private ApiClient.ServicesApiInterface servicesApiInterface;

    public RestDataStore() {
        servicesApiInterface= ApiClient.getMyApiClient();
    }

    @Override
    public void onUploadPhoto(final RepositoryCallback repositoryCallback) {
        int idUser = 2;
        servicesApiInterface.users(idUser, new Callback<PhotoResponse>() {
            @Override
            public void success(PhotoResponse placeResponse, Response response) {
                if(placeResponse!=null) {
                    repositoryCallback.onSuccess(placeResponse);
                }else{
                    repositoryCallback.onError("");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String message="";
                if(error!=null) {
                    message= error.getMessage();
                }
                Log.v(TAG,"error "+message);
                repositoryCallback.onError(message);
            }
        });
    }
}
