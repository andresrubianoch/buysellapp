package com.interactor;

import com.repository.ServerRepository;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class InteractorImpl {

    private final ServerRepository repository;

    public InteractorImpl(ServerRepository repository) {
        this.repository = repository;
    }

    public void onUploadPhotos(final ServerCallback signInCallback)
    {
        repository.uploadPhotos(signInCallback);
    }
}