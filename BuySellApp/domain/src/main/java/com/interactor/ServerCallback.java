package com.interactor;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/16.
 */
public interface ServerCallback {

    void onThemeSuccess();
    void onThemeError(String message);

}
