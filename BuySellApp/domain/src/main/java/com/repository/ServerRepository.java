package com.repository;

import com.interactor.ServerCallback;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public interface ServerRepository {

    void uploadPhotos(final ServerCallback albumCallback);

}
